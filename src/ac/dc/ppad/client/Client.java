package ac.dc.ppad.client;

import java.rmi.Naming;

import ac.dc.ppad.server.InterfaceInfo;

public class Client {

	public static void main(String[] args) {

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			
			InterfaceInfo server = (InterfaceInfo)Naming.lookup("//127.0.0.1:80/myServer");
			System.out.println("Server response: "+ server.getSomething("test"));
		
		} catch (Exception e) {
		
			System.err.println("Client error: ");
			e.printStackTrace();
			
		}
	}

}
